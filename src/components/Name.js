import React, {Component} from 'react';
import {connect} from "react-redux";
import {setName, getName} from "../actions/name";

class Name extends Component {

  constructor(props, context) {
    super(props, context);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.props.setName(event.target.value);
  }

  componentDidMount() {
    this.props.getName();
  }

  render() {
    return (
      <div>
        <h1>Hi, {this.props.name}</h1>
        <input type="text" onChange={this.handleChange}/>
      </div>
    );
  }
}

const mapStateToProps = ({nameReducer}) => ({
  name: nameReducer.name
});

const mapDispatchToProps = (dispatch) => ({
  setName: (name) => (dispatch(setName(name))),
  getName: (name) => (dispatch(getName(name))),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Name);